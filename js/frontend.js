$(document).ready(function(){
    $('.instagram-carousel').owlCarousel({
        loop:true,
        margin: 23,
        center: true,
        dots: false,
        mouseDrag:true,
        touchDrag:true,
        autoplay:true,
        responsive:{
            0:{
                items: 3,
                autoWidth: true
            }
        }

    });
    $('a.target-burger').click(function(e){
        $('.main-nav, a.target-burger').toggleClass('toggled');
        e.preventDefault();
    });//target-burger-click
});